import React from 'react';
import '../CSScomponent/Turn.css'

export default class Turn extends React.Component {
    render() {
        return (
            <div class='muaLuot'>
                <div class='itemMuaLuot turnBackground'>
                    <div class='yellowTextTurn fontSize18'>4</div>
                    <div class='yellowTextTurn'>Lượt quay chính</div>
                </div>
                <div onClick = {this.props.onClick} class='itemMuaLuot'>
                    <div>
                        <img src={require('../icon/btn_mua.webp')} alt='asd' width="120" height='50' />
                    </div>
                    <div class='buyTurn'>MUA LƯỢT</div>
                </div>
                <div class='itemMuaLuot turnBackground'>
                    <div class='yellowTextTurn fontSize18'>4</div>
                    <div class='yellowTextTurn'>Lượt quay ngày</div>
                </div>
            </div>
        )
    }
}