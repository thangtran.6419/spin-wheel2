import React from 'react';
import '../CSScomponent/Setting.css';
import { Link } from 'react-router-dom';

export default class Setting extends React.Component {

    render() {
        return (
            <div class='bodyST'>
                <div class='containerRuleSeT'>
                    <div class='headRuleST'>
                        <Link to='/' >
                            <div class='center' style = {{marginTop: 15}}>
                                <img src={require('../icon/back.png')} alt='asd' width="20" height='20' />
                            </div>
                        </Link>
                        <div class='center rule'>Cài đặt</div>
                    </div>
                    <div class='contentRuleST' style={{ backgroundColor: 'transparent', flexDirection: 'column', justifyContent: 'flex-start' }}>
                        <div class='soundViewST'>
                            <div class='textSound'>Âm thanh</div>
                            <div class='switchView'>
                                <label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class='soundViewST' style={{ height: 100, flexDirection: 'column', alignItems: 'flex-start' }}>
                            <div class='yellowText'>Điều chỉnh gói quay tự động</div>
                            <div class='whiteText' style ={{marginBottom: 20}}>Bạn chưa mua gói quay tự động, mua ngay?</div>
                        </div>

                        <div class='center'>
                            <img src={require('../icon/btn_yellow.webp')} alt='asd' style={{ marginLeft: 0, marginTop: 0 }} width="320" height='50' />
                            <div class='text_submitST' >MUA GÓI QUAY TỰ ĐỘNG</div>
                        </div>
                        <Link to='/' style = {{width: 300, backgroundColor:'red'}}>
                            <div class=' text_submitST ruleST' >Đóng</div>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}