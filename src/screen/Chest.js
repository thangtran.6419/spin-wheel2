import React from 'react';
import '../CSScomponent/Rule.css'
import { Link } from 'react-router-dom'

export default class Chest extends React.Component {

    render() {
        return (
            <div class = 'body'>
                <div class='containerRuleC'>
                    <div class='headRuleC'>
                        <Link to='/'>
                            <div class='center ' style = {{marginTop: 15}}>
                                <img src={require('../icon/back.png')} alt='asd' width="20" height='20' />
                            </div>
                        </Link>
                        <div class='center rule'>Danh sách trúng giải</div>
                    </div>
                    <div class='contentRuleC' style={{ marginTop: 50 }}>
                        <div class = 'image' style={{ marginLeft: 130, marginTop: -60 }}>
                            <img src={require('../icon/chest.webp')} alt='asd' width="200" height='150' />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}