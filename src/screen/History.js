import React from 'react';
import '../CSScomponent/Rule.css';
import { Link } from 'react-router-dom'

export default class History extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            spin: true
        }
    }

    render() {
        const backgroundColor = this.state.spin ? 'white' : null;
        const colorT = this.state.spin ? 'black' : null;
        const backgroundColorBuy = !this.state.spin ? 'white' : null;
        const colorBuy = !this.state.spin ? 'black' : null;
        return (
            <div class='bodyRule'>
                <div class='containerR' >
                    <div class='headRule'>
                        <Link to='/'>
                            <div class='center' style = {{marginTop: 15}}>
                                <img src={require('../icon/back.png')} alt='asd' width="20" height='20' />
                            </div>
                        </Link>
                        <div class='center rule'>Lịch sử</div>
                    </div>
                    <div class='headRuleHT backgroundTab'>
                        <div style={{ backgroundColor, color: colorT }} class='oneTab' onClick={() => this.setState({ spin: true })}>Quay Spin</div>
                        <div style={{ backgroundColor: backgroundColorBuy, color: colorBuy }} class='oneTab' onClick={() => this.setState({ spin: false })}>Mua lượt</div>
                    </div>
                    <div class='contentRuleHT borderContent' style={{ borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}>
                        {/* <img src={require('../icon/chest.webp')} alt='asd' width="200" height='150' /> */}
                    </div>
                </div>
            </div>
        )
    }
}