import React from 'react';
import '../CSScomponent/Foot.css'
import { Link } from 'react-router-dom'

export default class Foot extends React.Component {
    render() {
        return (
            <div class='footView'>
                <Link to='/History'>
                    <div class='itemFoot'>
                        <div>
                            <img src={require('../icon/history.webp')} alt='asd' width="20" height='20' />
                        </div>
                        <div class='whiteTextAS margin'>Lịch sử</div>
                    </div>
                </Link>
                <Link to='/Rule'>
                    <div class='itemFoot'>
                        <div>
                            <img src={require('../icon/rule.webp')} alt='asd' width="20" height='20' />
                        </div>
                        <div class='whiteTextAS margin'>Luật chơi</div>
                    </div>
                </Link>
                <Link to='/Setting'>
                    <div class='itemFoot'>
                        <div>
                            <img src={require('../icon/settings.webp')} alt='asd' width="20" height='20' />
                        </div>
                        <div class='whiteTextAS margin'>Tùy chỉnh</div>
                    </div>
                </Link>
                <div class='streamView'>
                    <div class='dot'></div>
                    <div class='whiteTextAS margin'>Stream</div>
                </div>
            </div>
        )
    }
}