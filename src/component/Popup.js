import React from 'react';
import '../CSScomponent/Popup.css';
import { Link } from 'react-router-dom';

class Popup extends React.Component {

    buyTurnUI = () => {
        const color2 = this.props.vnd === 20 ? 'white' : null;
        const color4 = this.props.vnd === 40 ? 'white' : null;
        const color6 = this.props.vnd === 60 ? 'white' : null;

        return (
            <div class='contentText buyturn' >
                <div onClick={this.props.vnd20Function} class='itemBuyTurn'>
                    {this.props.vnd === 20
                        ?
                        <div>
                            <img src={require('../icon/btn_itemBuyTurn.webp')} alt='asd' width="80" height='55' />
                        </div>
                        : null
                    }
                    <div class='kindOfTurn'>
                        <div class='yellowTextAS fontsize' style={{ color: color2 }}>2 SPIN</div>
                        <div class=' whiteTextAS fontsize'>20.000 VNĐ</div>
                    </div>
                </div>
                <div onClick={this.props.vnd40Function} class='itemBuyTurn'>
                    {this.props.vnd === 40
                        ?
                        <div>
                            <img src={require('../icon/btn_itemBuyTurn.webp')} alt='asd' width="80" height='55' />
                        </div>
                        : null
                    }
                    <div class='kindOfTurn'>
                        <div class='yellowTextAS fontsize' style={{ color: color4 }}>4 SPIN</div>
                        <div class='whiteTextAS fontsize'>40.000 VNĐ</div>
                    </div>
                </div>
                <div onClick={this.props.vnd60Function} class='itemBuyTurn'>
                    {this.props.vnd === 60
                        ?
                        <div>
                            <img src={require('../icon/btn_itemBuyTurn.webp')} alt='asd' width="80" height='55' />
                        </div>
                        : null
                    }
                    <div class='kindOfTurn'>
                        <div class='yellowTextAS fontsize' style={{ color: color6 }}>6 SPIN</div>
                        <div class='whiteTextAS fontsize'>60.000 VNĐ</div>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className='popup' style={{ opacity: 1 }}>
                <div className='popupInner'>
                    <div class='viewHeadImage'>
                        <img class='headImage' src={require('../icon/head_pop.webp')} alt='asd' width="320" height='50' />
                    </div>
                    <div class='titleText'>{this.props.TextTitlePopup}</div>
                    <div class='close_popup' onClick={this.props.closePopup} />
                </div>
                <div className='popupInner'>
                    <div >
                        <img src={require('../icon/bg.webp')} alt='asd' width="320" height='320' />
                    </div>

                    {this.props.isBuyTurn ? this.buyTurnUI() : null}

                    <div class='titleText1'>{this.props.titleText}</div>

                    {this.props.isChooseNum ?
                        <div class='ViewChooseNum'>
                            <div class='titleText top' style={{ marginTop: 60 }}>So kết quả với Sổ xố kiến thiết Miền Bắc</div>
                            <div class='inputNumber contentText'>
                                <div class='oneInput'>
                                </div>
                                <div class='oneInput'>
                                </div>
                            </div>
                        </div>
                        : null
                    }
                    <div class='contentText'>{this.props.contentText}</div>
                    <div onClick={this.props.closePopup} class='btn_submit'>
                        <img src={require('../icon/btn_yellow.webp')} alt='asd' width="250" height='40' />
                        <div class='text_submit'>XÁC NHẬN</div>
                    </div>
                    {this.props.isBuyTurn
                        ? <div class='luatchoiPP' onClick={this.props.AutoSpin}>{this.props.TextFootPopup}</div>
                        :
                        <Link to='/Rule' class='luatchoiPP'>
                            {/* <div class='luatchoiPP'> */}
                                <div class='borderRulePP'>
                                    {this.props.TextFootPopup}
                                </div>
                            {/* </div> */}
                        </Link>
                    }
                </div>
                <div className='popupInner imageFoot'>
                    <img src={require('../icon/foot_pop.webp')} alt='asd' style={{ marginLeft: 0, marginTop: 0 }} width="320" height='20' />
                </div>
            </div>
        );
    }
}

export default Popup;