import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { CSSTransition, TransitionGroup } from 'react-transition-group';
// import AppTest from './AppTest';
import Chest from './screen/Chest';
import Setting from './screen/Setting';
import Rule from './screen/Rule';
import History from './screen/History';
import * as serviceWorker from './serviceWorker';
import Spin from './screen/Spin'

const routing = (
    <Router>
        <Route
            render={({ location }) => {
                return (
                    <TransitionGroup>
                        <CSSTransition
                            // classNames="fade"
                            timeout={{
                                enter: 1000,
                            }}
                            classNames="example"
                            key={location.key}
                        >
                            <Switch location={location}>
                                <Route exact path="/" component={Spin} />
                                <Route path="/Chest" component={Chest} />
                                <Route path="/Setting" component={Setting} />
                                <Route path="/Rule" component={Rule} />
                                <Route path="/History" component={History} />
                            </Switch>
                        </CSSTransition>
                    </TransitionGroup>
                )
            }
            }
        />
    </Router >
)

ReactDOM.render(routing, document.getElementById('root'));

serviceWorker.unregister();
